import axios from "axios";
import getEnv from "./getEnv.util";

export async function api({
  url,
  method,
  data,
}: {
  url: string;
  method?: "post" | "get";
  data?: object;
}) {
  try {
    const result = await axios({
      url: `${getEnv("VITE_API_URL")}${url}`,
      method: method || "get",
      data,
    });
    return result.data;
  } catch {
    return undefined;
  }
}
