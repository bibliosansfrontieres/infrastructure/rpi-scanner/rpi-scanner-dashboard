declare global {
  interface Window {
    configs: {
      [key: string]: string;
    };
  }
}

export default function getEnv(name: string) {
  return window?.configs?.[name] || import.meta.env[name];
}
