import { ReactNode, useContext, useEffect, useState } from "react";
import { SoftwareEnum } from "../../../enums/software.enum";
import { IRpi } from "../../../interfaces/rpi.interface";
import CapitalizedMacAddress from "./CapitalizedMacAddress/CapitalizedMacAddress";
import UndefinedActions from "../Actions/UndefinedActions/UndefinedActions";
import OlipV1Actions from "../Actions/OlipV1Actions/OlipV1Actions";
import { LastBlink } from "../../../contexts/last-blink.context";

export default function Rpi({
  rpi,
  filter,
}: {
  rpi: IRpi;
  filter?: SoftwareEnum;
}) {
  const { lastBlink } = useContext(LastBlink);
  const [classNames, setClassNames] = useState("card-body");

  useEffect(() => {
    if (lastBlink === rpi.id) {
      setClassNames("card-body bd-last-blink");
    }
  }, [lastBlink, rpi.id]);

  const makeBlink = async () => {
    let bgBefore = false;
    let rounds = 0;
    while (rounds < 10) {
      console.log("rounds", rounds, bgBefore);
      if (bgBefore === true) {
        setClassNames("card-body");
        bgBefore = false;
      } else {
        setClassNames("card-body bg-red");
        bgBefore = true;
      }
      await new Promise((resolve) => setTimeout(resolve, 500));
      rounds++;
    }
    setClassNames("card-body bg-last-blink");
  };

  const displaySoftware = !filter;
  let macAddress: ReactNode = rpi.macAddress;

  if (filter && filter === SoftwareEnum.OLIP_V1) {
    macAddress = <CapitalizedMacAddress macAddress={rpi.macAddress} />;
  }

  let actions = <UndefinedActions />;

  switch (filter) {
    case SoftwareEnum.OLIP_V1:
      actions = <OlipV1Actions rpi={rpi} makeBlink={makeBlink} />;
      break;
  }

  return (
    <div className="card" style={{ textAlign: "center" }}>
      <div
        className={classNames}
        style={{ transition: " background-color 0.3s ease" }}
      >
        <h5 className="card-title">{rpi.hostname}</h5>
        <p className="card-text">
          IP : {rpi.ip}
          <br />
          Mac : {macAddress}
          {displaySoftware && (
            <>
              <br />
              Software : {rpi.software}
            </>
          )}
        </p>
        {actions}
      </div>
    </div>
  );
}
