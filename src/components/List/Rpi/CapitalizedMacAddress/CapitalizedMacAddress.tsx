export default function CapitalizedMacAddress({
  macAddress,
}: {
  macAddress: string;
}) {
  let macSplitted = macAddress.split(":");
  if (macSplitted.length < 2) macSplitted = ["ER", "RO", "R"];
  const second = macSplitted[macSplitted.length - 1].toUpperCase();
  const first = macSplitted[macSplitted.length - 2].toUpperCase();
  macSplitted = macSplitted.slice(0, -2);
  return (
    <>
      {macSplitted.join(":")}:
      <b>
        {first}:{second}
      </b>
    </>
  );
}
