import { useEffect, useState } from "react";
import { IRpi } from "../../interfaces/rpi.interface";
import { api } from "../../utils/api.utils";
import { SoftwareEnum } from "../../enums/software.enum";
import Rpi from "./Rpi/Rpi";

export default function List({ filter }: { filter?: SoftwareEnum }) {
  const [rpis, setRpis] = useState<IRpi[]>([]);

  useEffect(() => {
    const getRpis = async () => {
      let url = "/rpis";
      if (filter) {
        if (filter === SoftwareEnum.OLIP_V1) {
          url = `${url}?software=${SoftwareEnum.OLIP_V1}`;
        }
      }
      const result = await api({ url });
      if (result) {
        setRpis(result);
      }
    };

    getRpis();

    const timer = setInterval(() => {
      getRpis();
    }, 5000);

    return () => clearTimeout(timer);
  }, [filter]);

  return (
    <div style={{ margin: "0 auto", maxWidth: "800px" }}>
      {rpis.map((rpi) => {
        return <Rpi key={rpi.id} rpi={rpi} filter={filter} />;
      })}
    </div>
  );
}
