import {
  faClipboard,
  faLightbulb,
  faPowerOff,
  faRocket,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { IRpi } from "../../../../interfaces/rpi.interface";
import { toast } from "react-toastify";
import clipboardCopy from "clipboard-copy";
import { useContext, useState } from "react";
import { LastBlink } from "../../../../contexts/last-blink.context";
import { api } from "../../../../utils/api.utils";

export default function OlipV1Actions({
  rpi,
  makeBlink,
}: {
  rpi: IRpi;
  makeBlink: () => void;
}) {
  const [isShutdownDisabled, setIsShutdownDisabled] = useState(false);
  const [isDeployDisabled, setIsDeployDisabled] = useState(false);
  const [isBlinkDisabled, setIsBlinkDisabled] = useState(false);
  const { setLastBlink } = useContext(LastBlink);

  const blink = async () => {
    setIsBlinkDisabled(true);
    const result = await api({ url: `/rpis/blink/${rpi.id}` });
    if (result) {
      makeBlink();
      toast(`Blink order sent to ${rpi.ip}`, { type: "success" });
      setLastBlink(rpi.id);
    } else {
      makeBlink();
      setLastBlink(rpi.id);
      toast(`Error while sending blink order to ${rpi.ip}`, {
        type: "error",
      });
    }
    setIsBlinkDisabled(false);
  };

  const deploy = async () => {
    setIsDeployDisabled(true);
    const result = await api({ url: `/rpis/deploy/${rpi.id}` });
    if (result) {
      toast(`Deploy order sent to ${rpi.ip}`, { type: "success" });
      setLastBlink(rpi.id);
    } else {
      toast(`Error while sending deploy order to ${rpi.ip}`, {
        type: "error",
      });
    }
    setIsDeployDisabled(false);
  };

  const shutdown = async () => {
    setIsShutdownDisabled(true);
    const result = await api({ url: `/rpis/shutdown/${rpi.id}` });
    if (result) {
      toast(`Shutdown order sent to ${rpi.ip}`, { type: "success" });
      setLastBlink(rpi.id);
    } else {
      toast(`Error while sending shutdown order to ${rpi.ip}`, {
        type: "error",
      });
    }
    setIsShutdownDisabled(false);
  };

  const copyToClipboard = async () => {
    const stringToCopy = `Hostname : ${rpi.hostname}\n${
      rpi.serial !== null ? `Serial: ${rpi.serial}\n` : ""
    }Mac address : ${rpi.macAddress}\nIP address : ${rpi.ip}`;
    await clipboardCopy(stringToCopy);
    toast("Raspberry Pi informations successfully copied to clipboard !", {
      type: "success",
    });
  };

  return (
    <div style={{ display: "flex", justifyContent: "center" }}>
      <button
        className="btn btn-secondary mx-1"
        title="Copy to clipboard"
        onClick={copyToClipboard}
      >
        <FontAwesomeIcon icon={faClipboard} />
      </button>
      <button
        className="btn btn-primary mx-1"
        title="Make RPi blink"
        onClick={blink}
        disabled={isBlinkDisabled}
      >
        <FontAwesomeIcon icon={faLightbulb} />
      </button>
      {!rpi.isDeployed && (
        <button
          className="btn btn-warning mx-1"
          title="Deploy OLIP v1"
          onClick={deploy}
          disabled={isDeployDisabled}
        >
          <FontAwesomeIcon icon={faRocket} />
        </button>
      )}
      <button
        className="btn btn-danger mx-1"
        title="Shutdown RPi"
        onClick={shutdown}
        disabled={isShutdownDisabled}
      >
        <FontAwesomeIcon icon={faPowerOff} />
      </button>
    </div>
  );
}
