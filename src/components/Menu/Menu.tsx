import MenuLink from "./MenuLink/MenuLink";

export default function Menu() {
  return (
    <ul className="nav justify-content-center nav-pills my-3">
      <MenuLink to={"/"}>Raspberry Pis</MenuLink>
      <MenuLink to={"/olip-v1"}>OLIP v1</MenuLink>
    </ul>
  );
}
