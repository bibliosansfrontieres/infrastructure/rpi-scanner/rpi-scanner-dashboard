import { ReactNode } from "react";
import { Link, useLocation } from "react-router-dom";

export default function MenuLink({
  children,
  to,
}: {
  children: ReactNode;
  to: string;
}) {
  const { pathname } = useLocation();
  const classes = ["nav-link"];

  if (pathname === to) {
    classes.push("active");
  }

  return (
    <li className="nav-item">
      <Link to={to} className={classes.join(" ")}>
        {children}
      </Link>
    </li>
  );
}
