import List from "../components/List/List";
import { SoftwareEnum } from "../enums/software.enum";

export default function OlipV1Page() {
  return <List filter={SoftwareEnum.OLIP_V1} />;
}
