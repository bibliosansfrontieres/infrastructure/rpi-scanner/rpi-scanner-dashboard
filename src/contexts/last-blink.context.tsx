import {
  createContext,
  Dispatch,
  ReactNode,
  SetStateAction,
  useState,
} from "react";

type ContextValue = {
  lastBlink: number | undefined;
  setLastBlink: Dispatch<SetStateAction<number | undefined>>;
};

const defaultValue = {
  lastBlink: undefined,
  setLastBlink: () => {
    /* expected empty method */
  },
};

export const LastBlink = createContext<ContextValue>(defaultValue);

export const LastBlinkContext = ({ children }: { children: ReactNode }) => {
  const [lastBlink, setLastBlink] = useState<number | undefined>(undefined);

  const contextValue = {
    lastBlink,
    setLastBlink,
  };

  return (
    <LastBlink.Provider value={contextValue}>{children}</LastBlink.Provider>
  );
};
