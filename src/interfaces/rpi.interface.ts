import { SoftwareEnum } from "../enums/software.enum";

export interface IRpi {
  id: number;
  ip: string;
  macAddress: string;
  name: string | null;
  hostname: string | null;
  isAlive: boolean;
  isDeployed: boolean;
  software: SoftwareEnum;
  serial: string | null;
}
