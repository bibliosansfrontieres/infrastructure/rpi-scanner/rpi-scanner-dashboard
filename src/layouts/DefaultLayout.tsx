import { Outlet } from "react-router-dom";
import Menu from "../components/Menu/Menu";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default function DefaultLayout() {
  return (
    <>
      <Menu />
      <Outlet />
      <ToastContainer />
    </>
  );
}
