import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.min.js";
import { RouterProvider } from "react-router-dom";
import router from "./router.tsx";
import { LastBlinkContext } from "./contexts/last-blink.context.tsx";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <LastBlinkContext>
      <RouterProvider router={router} />
    </LastBlinkContext>
  </React.StrictMode>
);
