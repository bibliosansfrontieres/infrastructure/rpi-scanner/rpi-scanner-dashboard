###############
# BUILD STAGE #
###############

FROM node:18-alpine AS build

# Add more timeouts and retries to npm because parallel builds can fail
RUN npm config set fetch-retries 3
RUN npm config set fetch-retry-mintimeout 300000
RUN npm config set fetch-retry-maxtimeout 600000
RUN npm config set fetch-timeout 300000

# Install install only express and dotenv for server.js in /app
WORKDIR /app
RUN npm install express dotenv

# Install dev node modules in /build
WORKDIR /build
COPY package.json package-lock.json ./
RUN npm install

# Build project in /build
COPY . .
RUN npm run build

####################
# PRODUCTION STAGE #
####################

FROM node:18-alpine AS production

WORKDIR /app

# Copy node modules for server.js
COPY --from=build /app/node_modules /app/node_modules

# Copy builded node project
COPY --from=build /build/dist /app/dist
COPY --from=build /build/public /app/public
COPY --from=build /build/server.js /app/server.js
COPY package.json /app

ENV NODE_ENV production

# Run entrypoint to update env variables at runtime
COPY entrypoint.sh /app
RUN chmod +x /app/entrypoint.sh
ENTRYPOINT ["/app/entrypoint.sh"]

CMD [ "node", "server.js" ]
