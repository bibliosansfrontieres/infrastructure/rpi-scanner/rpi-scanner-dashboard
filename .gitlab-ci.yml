stages:
  - Dependencies
  - Tests
  - Containers

default:
  image: node:18-alpine
  tags:
    - k8s
  artifacts:
    expire_in: 30m

variables:
  IMAGE_NAME: "rpi-scanner-dashboard"
  TEST_TAG: "$CI_COMMIT_SHORT_SHA"
  RELEASE_TAG: "latest"
  VERSION_PATH: "https://gitlab.com/bibliosansfrontieres/infrastructure/rpi-scanner/rpi-scanner-dashboard/-/raw/main/package.json"

  DOCKER_REGISTRY: "index.docker.io"
  DOCKER_CONTAINER_IMAGE: "$DOCKER_REGISTRY/$DOCKER_HUB_REPOSITORY/$IMAGE_NAME"
  DOCKER_TEST_IMAGE: "$DOCKER_CONTAINER_IMAGE:$TEST_TAG"
  DOCKER_RELEASE_IMAGE: "$DOCKER_CONTAINER_IMAGE:$RELEASE_TAG"
  GITLAB_CONTAINER_IMAGE: "$CI_REGISTRY_IMAGE"
  GITLAB_TEST_IMAGE: "$GITLAB_CONTAINER_IMAGE:$TEST_TAG"
  GITLAB_RELEASE_IMAGE: "$GITLAB_CONTAINER_IMAGE:$RELEASE_TAG"

  # k8s generic variables - you usually don't need to override these
  DOCKER_TLS_CERTDIR: "/certs"
  DOCKER_HOST: tcp://127.0.0.1:2376
  DOCKER_TLS_VERIFY: 1
  DOCKER_CERT_PATH: "${DOCKER_TLS_CERTDIR}/client"
  DOCKER_VERSION: 20.10.12
  BUILDX_VERSION: 0.5.1

  # Build architectures with RPI
  #BUILD_ARCH_LIST: linux/amd64,linux/arm64
  # Build architectures without RPI
  BUILD_ARCH_LIST: linux/amd64

Install:
  stage: Dependencies
  script:
    - npm install
  artifacts:
    paths:
      - node_modules
    expire_in: 30m

Build:
  stage: Tests
  script:
    - npm run build
  dependencies:
    - Install

Lint:
  stage: Tests
  script:
    - npm run lint
  dependencies:
    - Install

Format:
  stage: Tests
  script:
    - npm run format:check
  dependencies:
    - Install

Containers:
  stage: Containers
  image: docker:${DOCKER_VERSION}
  services:
    - name: docker:${DOCKER_VERSION}-dind
      command: ["--experimental"]
  before_script:
    - mkdir --parent /usr/lib/docker/cli-plugins/
    - wget --output-document /usr/lib/docker/cli-plugins/docker-buildx https://github.com/docker/buildx/releases/download/v${BUILDX_VERSION}/buildx-v${BUILDX_VERSION}.linux-amd64
    - chmod a+rx /usr/lib/docker/cli-plugins/docker-buildx
    - docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
    - docker version
    - docker login --username "$DOCKER_HUB_USER" --password "$DOCKER_HUB_PASSWORD"
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - apk update && apk add jq
    - export VERSION=`jq -r ".version" < ./package.json`
    - export DOCKER_VERSIONED_IMAGE="$DOCKER_CONTAINER_IMAGE:$VERSION"
    - export GITLAB_VERSIONED_IMAGE="$GITLAB_CONTAINER_IMAGE:$VERSION"
    - docker pull $DOCKER_VERSIONED_IMAGE || true
    - docker pull $GITLAB_VERSIONED_IMAGE || true
    - docker context create olip
    - docker buildx create olip --use
    - docker buildx inspect --bootstrap
    - |
      docker buildx build \
        --platform ${BUILD_ARCH_LIST} \
        --label "org.opencontainers.image.authors=Bibliothèques Sans Frontières" \
        --label "org.opencontainers.image.title=$CI_PROJECT_TITLE" \
        --label "org.opencontainers.image.description=$CI_PROJECT_DESCRIPTION" \
        --label "org.opencontainers.image.url=$CI_REGISTRY_IMAGE" \
        --label "org.opencontainers.image.source=$CI_PROJECT_URL" \
        --label "org.opencontainers.image.created=$CI_JOB_STARTED_AT" \
        --label "org.opencontainers.image.revision=$CI_COMMIT_SHA" \
        --label "org.opencontainers.image.ref.name=$CI_COMMIT_REF_NAME" \
        --label "org.opencontainers.image.version=$CI_COMMIT_REF_NAME" \
        -t ${DOCKER_RELEASE_IMAGE} \
        -t ${DOCKER_VERSIONED_IMAGE} \
        -t ${DOCKER_TEST_IMAGE} \
        -t ${GITLAB_RELEASE_IMAGE} \
        -t ${GITLAB_VERSIONED_IMAGE} \
        -t ${GITLAB_TEST_IMAGE} \
        . \
        --push
  dependencies: []
  only:
    - main
